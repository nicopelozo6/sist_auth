import { Response, Request } from "express";
import { handlerError } from "../../utils/error.handler";
import { getUsersService } from "./service";

const getUsersCtrl = async(_req:Request, res:Response) => {
    try{
        let response = await getUsersService();
        if(response.r.c==1)
            res.status(400).send(response)
        else
            res.status(200).send(response)
    }catch(e){
        handlerError(res,"An error occurred",e,500)
    }
}

export { getUsersCtrl }