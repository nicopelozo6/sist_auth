import { IResponse } from "../../utils/response.interface"
import { UserModel } from "./model"

const getUsersService = async () => {
    let users = await UserModel.find().select("-password -createdAt -updatedAt -__v")
    let response:IResponse= {r:{c:0,m:'Ok',d:users}}
    return response
}

export { getUsersService }