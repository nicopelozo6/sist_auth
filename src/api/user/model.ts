import {Schema, Document, model, PaginateModel} from "mongoose";
import paginate from 'mongoose-paginate-v2'


export interface IUserType{
    code:number,
    name:string
}

export interface IUser{
    name: string,
    email:string,
    password:string,
    birthday:Date,
    userType:IUserType,
    age:number
}


export const UserSchema = new Schema({
    name: {
        type:String,
        required:true
    },
    email:{
        type:String,
        require:true
    },
    password:{
        type:String,
        require:String
    },
    birthday:{
        type:Date,
        require:true
    },
    age:{
        type:Number,
        require:true
    }/* ,
    typeUser:{
        type:Object,
        require:true
    } */
},
{
    timestamps: true,
    versionKey:false
})

UserSchema.plugin(paginate)
interface UserDocument extends Document, IUser{}

export const UserModel = model<UserDocument,PaginateModel<UserDocument>>('user', UserSchema, 'users')
