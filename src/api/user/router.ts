import { Router } from "express";
import { checkJwt } from "../../middlewares/session";
import { getUsersCtrl } from "./controller";

/* testing */
const router = Router()

router.get('/getUsers', checkJwt ,getUsersCtrl)

export default router