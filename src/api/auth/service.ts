import { encrypt, verified } from "../../utils/bcrypt.handler";
import { generateToken } from "../../utils/jwt.handler";
import { IResponse } from "../../utils/response.interface";
import { IUser, UserModel } from "../user/model";


const signUpService = async (userData: IUser) => {
    const checkIs = await UserModel.findOne({ email: userData.email })
    if (checkIs)
        return { r: { c: 1, m: "This email already exists." } }

    let birthday = new Date(userData.birthday)
    let passHash = await encrypt(userData.password)

    const registerNewUser = new UserModel({
        email: userData.email,
        password: passHash,
        name: userData.name,
        birthday: birthday,
        age:userData.age
    })
    let userSaved = await registerNewUser.save()
    let response: IResponse
    if (userSaved)
        response = { r: { c: 0, m: "User created successfully",d:{email:userSaved.email,name:userSaved.name,age:userSaved.age,birthday:userSaved.birthday}} }
    else
        response = { r: { c: 1, m: "Error creating user", d: {} } }
    return response
}

const loginService = async (email: string, password: string) => {
    let exists = await UserModel.findOne({ email: email });
    if (!exists)
        return { r: { c: 1, m: 'This user does not exist' } }
    let passwordHash = exists.password
    let isCorrect = await verified(password, passwordHash)

    let response: IResponse
    if (!isCorrect)
        response = { r: { c: 1, m: 'Password incorrect.'  } };
    else{
        let token = await generateToken(exists.email)
        response = { r: { c: 0, m: 'Welcome!!!',d: token } };
    }
        
    return response
}

export { signUpService, loginService }