import { Router } from "express"
import { loginCtrl, signUpCtrl } from "./controller"
import { validateCreate, validateLogin } from "../../middlewares/validators/user.validator";

const router = Router()

router.post("/signup",validateCreate,signUpCtrl);
router.post('/login',validateLogin,loginCtrl)

export default router