import { Request, Response } from "express"
import { loginService, signUpService } from "./service";
import { handlerError } from "../../utils/error.handler";
const signUpCtrl = async(req:Request, res:Response)=>{
    try{
        let userData= req.body;
        let response = await signUpService(userData)
        if(response.r.c==1)
            res.status(400).send(response)
        else
            res.status(200).send(response)
    }
    catch(e){
        handlerError(res,"An error occurred",e,500)
    }
}

const loginCtrl = async({body}:Request,res:Response) => {
    try{
        let { email, password } = body;
        const response = await loginService(email,password);
        if(response.r.c==1)
            res.status(400).send(response)
        else
            res.status(200).send(response)
    }
    catch(e){
        handlerError(res,"An error occurred",e,500)
    }
}



export { loginCtrl, signUpCtrl}