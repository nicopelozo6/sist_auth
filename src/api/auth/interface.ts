import { Request } from "express"
import { IUser } from "../user/model"
interface RequestExt extends Request{
    user?: IUser
}
export { RequestExt }