import "dotenv/config" 
import express, { json } from "express"
import cors from "cors"
import dbConnect from "./config/mongoose"

const PORT = process.env.PORT  || 2000
const app = express()
app.use(cors())
app.use(json())

import auth from "./api/auth/routes"
app.use('/auth/',auth)

import user from './api/user/router'
app.use("/users", user);

dbConnect()

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`))