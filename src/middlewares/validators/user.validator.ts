import { NextFunction, Request, Response } from "express";
import { check } from "express-validator";
import { validateResult } from "../../utils/validateHelper";

export const validateCreate = [
    check('name')
    .exists()
    .notEmpty(),
    check("email")
    .exists()
    .isEmail()
    .notEmpty(),
    check("password")
    .exists()
    .notEmpty()
    .isLength({min:8}),
    check('birthday')
    .exists()
    .notEmpty(),
    check('age')
    .exists()
    .notEmpty()
    .isNumeric()
    .custom((value:any)=>{
        if(value<18){
            throw new Error("Age requirement not met");
        }
        return true
    }),
    (req:Request,res:Response,next:NextFunction) => {
        validateResult(req,res,next)
    }
]

export const validateLogin= [
    check("email")
    .exists()
    .isEmail()
    .notEmpty(),
    check("password")
    .exists()
    .notEmpty()
    .isLength({min:8}),
    (req:Request,res:Response,next:NextFunction) => {
        validateResult(req,res,next)
    }
]