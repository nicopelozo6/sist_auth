import { NextFunction, Response } from "express"
import { handlerError } from "../utils/error.handler"
import { verifyToken } from "../utils/jwt.handler"
import { RequestExt } from "../api/auth/interface"
import { IUser, UserModel } from "../api/user/model"

const checkJwt = async (req: RequestExt, res: Response, next: NextFunction) => {
    try {
        const jwtByUser = req.headers.authorization || ''
        const jwt = jwtByUser.split(' ').pop()
        let isUser = await verifyToken(`${jwt}`)
        let email = JSON.stringify(isUser).split('"')[3]
        const us = await UserModel.findOne({ email: email }).select("-password -createdAt -updatedAt -__v")
        if (!us) {
            res.status(401).send({ r: { c: 1, m: 'Usuario no encontrado' } })
        }
        req.user = <IUser>us
        next()
    } catch (e) {
        handlerError(res, "Invalid JWT", e, 400)
    }

}

export { checkJwt }