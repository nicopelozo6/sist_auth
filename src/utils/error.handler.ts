import { Response } from "express"

const handlerError = (res:Response, error:string,errorRaw:any,status:number) => {
    console.log("Errors: ",errorRaw)
    res.status(status);
    res.send({error})
}

export { handlerError } ;
