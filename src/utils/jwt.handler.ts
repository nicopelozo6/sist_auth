import { sign, verify } from 'jsonwebtoken'
const JWT_SECRET = process.env.JWT_SECRET || "tokenSistAuth"

const generateToken = async (id: string) => {
    const jwt = await sign({ id }, JWT_SECRET, {
        expiresIn: "2h",
    });
    return jwt

}

const verifyToken = async (jwt: string) => {
    
    let isOk = await verify(jwt, JWT_SECRET)
    return isOk
}

export { generateToken, verifyToken }