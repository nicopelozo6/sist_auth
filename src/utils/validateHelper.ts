import { NextFunction, Request, Response } from 'express'
import { validationResult } from 'express-validator'
import { handlerError } from './error.handler'

export const validateResult = (req: Request, res: Response, next: NextFunction) => {
    try {
        validationResult(req).throw()
        return next()
    } catch (e:any) {
        handlerError(res,e.array(),e.array(),403)
    }
}