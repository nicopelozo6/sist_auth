import { compare, hash } from "bcryptjs"

const encrypt = async (password:string) => {
    let passwordHash = await hash(password,8);
    return passwordHash
}

const verified = async(password:string,passHash:string) => {
    const isMatch=await compare(password, passHash);
    return isMatch
}

export { encrypt, verified }