export interface IResponse{
    r:{
        c:0|1, //0 = Is Ok, 1 = There was a mistake
        m:string,
        d?:any
    }
}